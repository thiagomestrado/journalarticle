class ChainRelations:
    def __init__(self, hostElement, elementInChain, face):
        self.hostElement = hostElement
        self.elementInChain = elementInChain
        self.face = face #The natural face of the elements being crossed is the way they are in the puzzle. It's intuitive, the exit of the element gives the face.