def initSocket(port):
    import zmq

    #print("Connecting to the environment")
    context = zmq.Context()
    socket = context.socket(zmq.REQ)

    address = "tcp://localhost:"
    address += str(port)
    socket.connect(address)

    return socket

################################################################################
def initState(socket):
    socket.send_string('init')
    state = socket.recv_string()
    end = False

    return state, end


################################################################################
def getConf(socket):
    socket.send_string('conf')
    message = socket.recv_string()
    message = message.split()

    minimumReward = float(message.pop(-1))

    maximumReward = float(message.pop(-1))

    actions = message
    
    return maximumReward, minimumReward, actions


################################################################################
def sendMessage(state, actionToPerform, socket):
    message = ''
    message = state + ' ' + actionToPerform

    socket.send_string(message)
    #print("State: " + str(state) + " and Action: " + str(actionToPerform))
    return message


################################################################################
def receiveMessage(socket):
    message = socket.recv_string()
    #print("R: %s " % message)
    #print("Message from Environment: %s" % message)
    newState, reward, terminalState, forbiddenActionPerformed, nonDeterministicExecuted = message.split()
    rew = float(reward)
    end = eval(terminalState)
    forbiddenActionPerformed = eval(forbiddenActionPerformed)
    nonDeterministicExecuted = eval(nonDeterministicExecuted)

    return newState, rew, end, forbiddenActionPerformed, nonDeterministicExecuted

################################################################################
def sendEndEpisode(socket):
    message = "END"
    socket.send_string(message)


################################################################################
