from Elements.Element import *
from enumerations import ElementType
from enviromentDefinition import *
import enviromentDefinition
from utilMethods import *
import utilMethods

class Post(Element):
    def __init__(self, nameOfElement, initialIndex, orderInPuzzle):
        Element.__init__(self, ElementType.POST, orderInPuzzle, True)
        self.name = nameOfElement
        self.initialIndex = initialIndex
        self.orderInPuzzle = orderInPuzzle        
    
    def printElementType(self):
        print("This is an element of type POST")
        
    def executeAction(self, elementReceivingAction, elementFace, currentState):   
        if elementReceivingAction.elementType == ElementType.RING:
            entitiesOfAction = (self, elementReceivingAction, elementFace)
            
            continueExecution = self.verifyRestrictions(elementReceivingAction, elementFace, currentState)
            if(not continueExecution):
                return (True, currentState, False) 
            
            util = UtilMethods()
            complexPrologResponse = util.executePrologCommand(currentState, entitiesOfAction)
            actionExecuted, newState = complexPrologResponse
            
            if(actionExecuted):
                if(not self.verifyNumberOfOcurrences(elementReceivingAction, currentState, newState)):
                    return (True, currentState, False)                
                
                if(newState != currentState):
                    return (False, newState, False)

            return (True, currentState, False)   
        
        return (True, currentState, True)
    
    def verifyRestrictions(self, elementReceivingAction, elementFace, currentState):   
        stringRelations, postRelations = currentState
        
        #Verify if the post is crossing the ring and the face of the crossing. If this is true, then it can't continue, because it can't cross the ring in the same direction more than once.
        for relation in postRelations:
            if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle and
               relation.face == elementFace):
                return False
           
        return True
    
    def verifyNumberOfOcurrences(self, elementReceivingAction, currentState, newState):
        stringRelations, postRelations = currentState
        newStringRelations, postRelations = newState
        
        numberOfOcurrencesAfterAction = 0
        for relation in newStringRelations:
            if(relation.elementInChain.orderInPuzzle == enviromentDefinition.ring.orderInPuzzle):
                numberOfOcurrencesAfterAction += 1
        
        if(numberOfOcurrencesAfterAction > enviromentDefinition.numberOfSphereCrossingRing):
            return False
        
        return True
    
            