###############################################################################

## Imports
from enumerations import  ElementFace
from Elements.Disk import *
from Elements.MainString import *
from Elements.Post import *
from Elements.Ring import *
from Elements.Sphere import *
from chainRelations import ChainRelations


# Creating the elements of the puzzle.
post = Post("post", 0, 0)
mainString = MainString("str", 14, 1)
ring = Ring("ring", 28, 2)
disk1 = Disk("disk1", 42, 3)
disk2 = Disk("disk2", 156, 4)
sphere1 = Sphere("sphere1", 70, 5)
sphere2 = Sphere("sphere2", 84, 6)


# Initial State
# For now the initial state is going to be a constant state, pre-defined by the programmer.
# Creating Initial Relationships

stringRelations = [ChainRelations(mainString, mainString, ElementFace.NEGATIVE),
                   ChainRelations(mainString, sphere1, ElementFace.POSITIVE),
                   ChainRelations(mainString, post, ElementFace.POSITIVE),
                   ChainRelations(mainString, sphere2, ElementFace.POSITIVE),
                   ChainRelations(mainString, mainString, ElementFace.POSITIVE)]

#Post is crossing the Ring.
postRelations = [ChainRelations(post, post, ElementFace.NEGATIVE),
                 ChainRelations(post, ring, ElementFace.POSITIVE),
                 ChainRelations(post, post, ElementFace.POSITIVE)]

initialState =  (stringRelations.copy(), postRelations.copy())
currentComplexState = (stringRelations.copy(), postRelations.copy())


# Goal State
# The Goal State is a state where the Ring isn't being crossed by any other element.
def isGoalState(completeStateToVerify):
    stringRelationsToVerify, postRelationsToVerify = completeStateToVerify
    
    for relation in stringRelationsToVerify: #In the position zero it's the string relations
        if int(relation.elementInChain.orderInPuzzle) == int(ring.orderInPuzzle):
            return False #If the ring is being crossed by the MainString, then it's not the goal state.
    
    for relation in postRelationsToVerify: #In the position one it's the post relations
        if int(relation.elementInChain.orderInPuzzle) == int(ring.orderInPuzzle): 
            return False #If the ring is being crossed by the Post, then it's not the goal state.
        
    #If the ring isn't being crossed by the MainString nor the Post, then it's free from the system.
    return True     

def isGoalStateFromString(stringStateToVerify):
    if(stringStateToVerify.find("2") > -1):
        return False

    return True
    

#Define actions with only the elements of the puzzle that can be crossed and can cross.
numberOfCrossingElements = 6
numberOfHoledElements = 2
numberOfFaces = 2

#Defining only possible actions.
totalNumberOfActions = 20
actions = list(range(0, totalNumberOfActions))
actionsDescription = []

#Post Actions
postTrhoughRingPos = "" + str(post.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
postTrhoughRingNeg = "" + str(post.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(postTrhoughRingPos) #0
actionsDescription.append(postTrhoughRingNeg) #1

#String Disk1 Actions
stringDisk1TrhoughPostPos = "" + str(disk1.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.POSITIVE.value))
stringDisk1TrhoughPostNeg = "" + str(disk1.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(stringDisk1TrhoughPostPos) #2
actionsDescription.append(stringDisk1TrhoughPostNeg) #3

#String Disk2 Actions
stringDisk2TrhoughPostPos = "" + str(disk2.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.POSITIVE.value))
stringDisk2TrhoughPostNeg = "" + str(disk2.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(stringDisk2TrhoughPostPos) #4
actionsDescription.append(stringDisk2TrhoughPostNeg) #5

#Sphere1 Actions
sphere1TrhoughRingPos = "" + str(sphere1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere1TrhoughRingNeg = "" + str(sphere1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere1TrhoughRingPos) #6
actionsDescription.append(sphere1TrhoughRingNeg) #7

#Sphere2 Actions
sphere2TrhoughRingPos = "" + str(sphere2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere2TrhoughRingNeg = "" + str(sphere2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere2TrhoughRingPos) #8
actionsDescription.append(sphere2TrhoughRingNeg) #9

#Ring Actions
ringTrhoughPostPos = "" + str(ring.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.POSITIVE.value))
ringTrhoughPostNeg = "" + str(ring.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(ringTrhoughPostPos) #10
actionsDescription.append(ringTrhoughPostNeg) #11



#Impossible actions

#Sphere1 Actions
sphere1TrhoughPostPos = "" + str(sphere1.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere1TrhoughPostNeg = "" + str(sphere1.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere1TrhoughPostPos) #12
actionsDescription.append(sphere1TrhoughPostNeg) #13

#Sphere2 Actions
sphere2TrhoughPostPos = "" + str(sphere2.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere2TrhoughPostNeg = "" + str(sphere2.orderInPuzzle) + str(post.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere2TrhoughPostPos) #14
actionsDescription.append(sphere2TrhoughPostNeg) #15


#String Disk1 Actions
stringDisk1TrhoughRingPos = "" + str(disk1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
stringDisk1TrhoughRingNeg = "" + str(disk1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(stringDisk1TrhoughRingPos) #16
actionsDescription.append(stringDisk1TrhoughRingNeg) #17

#String Disk2 Actions
stringDisk2TrhoughRingPos = "" + str(disk2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
stringDisk2TrhoughRingNeg = "" + str(disk2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(stringDisk2TrhoughRingPos) #18
actionsDescription.append(stringDisk2TrhoughRingNeg) #19

###############################################################################
# transition probabilities
oposite = 0
orthopos = 0
orthoneg = 0

aO = [0, 0, 0]
aP = [0, 0, 0]
aN = [0, 0, 0]

###############################################################################
# reward values
minimalReward  = -100 #If each action in that episode is the bad one, then the total reward will be -1000.
maximumReward  = 1000
rewardPerStep = -1

###############################################################################
episodes = 6000
trials = 30
port = 5030

###############################################################################
separador = "b"

###############################################################################
#This, together with the number of steps, is the limiting factor of the size of the string. With less steps, less crossings.
numberOfStringCrossingPost = 6
numberOfSphereCrossingRing = 6
allowedNumberOfKnots = 2
deterministic = True

#Non Deterministic Values:
determinismExecutedAction = 0.8 #The normal action the agent tried to execute
determinismOppositeAction = 0.9 #The action with the opposite face.
determinismNoAction = 1 #Stay in the same state


###############################################################################
puzzleDefinitionProlog = "Oracle/fishermansFollyDefinition.pl"
puzzlePlannerProlog = "Oracle/fishermansFollyPlanner.pl"
