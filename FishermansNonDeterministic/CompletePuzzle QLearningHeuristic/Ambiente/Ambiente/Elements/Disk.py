'''from Element import *'''
from Elements.Element import *
from enumerations import ElementType
from enviromentDefinition import *
import enviromentDefinition
from utilMethods import *
import utilMethods


class Disk(Element):
    def __init__(self, nameOfElement, initialIndex, orderInPuzzle):
        Element.__init__(self, ElementType.DISK, orderInPuzzle, False)
        self.name = nameOfElement
        self.initialIndex = initialIndex
    
    def printElementType(self):
        print("This is an element of type DISK")
        
    def executeAction(self, elementReceivingAction, elementFace, currentState):        
        #The disk can cross only the Post, any other element it's a forbidden action.    
        if elementReceivingAction.elementType == ElementType.POST: 
            entitiesOfAction = (self, elementReceivingAction, elementFace)
            
            continueExecution = self.verifyRestrictions(elementReceivingAction, elementFace, currentState)
            if(not continueExecution):
                return (True, currentState, False) 
            
            util = UtilMethods()
            complexPrologResponse = util.executePrologCommand(currentState, entitiesOfAction)
            actionExecuted, newState = complexPrologResponse
            
            if(actionExecuted):                
                if(not self.verifyNumberOfOcurrences(elementReceivingAction, currentState, newState)):
                    return (True, currentState, False)                
                
                if(self.identifyKnot(newState)):
                    return (True, currentState, False)
                
                if(newState != currentState):
                    return (False, newState, False)

            return (True, currentState, False)
                
        #If the action can't be executed, then it returns true indicating forbidden action and the current state without any change.
        return (True, currentState, True) 
    
    
    def verifyRestrictions(self, elementReceivingAction, elementFace, currentState):   
        stringRelations, postRelations = currentState
        
        #This is a restriction for the heuristic puzzle. Verify if the string is already crossing the Post multiple times, if it's, then this action will only make knots, which we don't want.                  
        '''numberOfPostCrossings = 0
        for relation in stringRelations:
            if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle):
                numberOfPostCrossings += 1
                
        if(numberOfPostCrossings > 2):
            return False
        '''
        #This verification has to be done when the new state is coming back, because the action being executed may be an undo.
        
        return True    
    
    def verifyNumberOfOcurrences(self, elementReceivingAction, currentState, newState):
        newStringRelations, postRelations = newState

        numberOfOcurrencesAfterAction = 0
        for relation in newStringRelations:
            if(relation.elementInChain.orderInPuzzle == enviromentDefinition.post.orderInPuzzle):
                numberOfOcurrencesAfterAction += 1
        
        #The string is not infinity in this puzzle.
        if(numberOfOcurrencesAfterAction > enviromentDefinition.numberOfStringCrossingPost):
            return False
        
        
        return True


    def identifyKnot(self, newState):
        stringRelations, postRelations = newState
        
        numberOfKnots = 0
        
        currentElementToEvaluate = None
        for relation in stringRelations:
            #Only consider the post elements, because it's with them that knots are created.
            if(relation.elementInChain.orderInPuzzle == enviromentDefinition.post.orderInPuzzle):
                if(currentElementToEvaluate is None): #If it's the first post element identified.
                    currentElementToEvaluate = relation
                else:
                    #If it's another post on the sequence, then it doesn't create a knot.
                    if(relation.elementInChain.orderInPuzzle != currentElementToEvaluate.elementInChain.orderInPuzzle):
                        currentElementToEvaluate = relation
                    else:
                        #If it's the same post on the sequence but with different face, then it doesn't create a knot.
                        if(relation.face != currentElementToEvaluate.face):
                            currentElementToEvaluate = relation
                        else: #Knot identified!!!!
                            #return True
                            numberOfKnots += 1
                            currentElementToEvaluate = relation

        if(numberOfKnots > enviromentDefinition.allowedNumberOfKnots):
            return True
        
        #If a knot is NOT identified
        return False
