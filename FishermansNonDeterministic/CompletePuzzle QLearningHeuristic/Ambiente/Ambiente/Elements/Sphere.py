from Elements.Element import *
from enumerations import ElementType
from enviromentDefinition import *
import enviromentDefinition
from utilMethods import *
import utilMethods

class Sphere(Element):
    def __init__(self, nameOfElement, initialIndex, orderInPuzzle):
        Element.__init__(self, ElementType.SPHERE, orderInPuzzle, False)
        self.name = nameOfElement
        self.initialIndex = initialIndex
        self.orderInPuzzle = orderInPuzzle        
    
    def printElementType(self):
        print("This is an element of type SPHERE")
        
    def executeAction(self, elementReceivingAction, elementFace, currentState):  
        if (elementReceivingAction.elementType == ElementType.RING):
            
            entitiesOfAction = (self, elementReceivingAction, elementFace)

            continueExecution = self.verifyRestrictions(elementReceivingAction, elementFace, currentState)
            if(not continueExecution):
                return (True, currentState, False) 

            util = UtilMethods()
            complexPrologResponse = util.executePrologCommand(currentState, entitiesOfAction)
            actionExecuted, newState = complexPrologResponse
            
            if(actionExecuted):
                if(not self.verifyNumberOfOcurrences(elementReceivingAction, currentState, newState)):
                    return (True, currentState, False)
                
                if(newState != currentState):
                    return (False, newState, False)

            return (True, currentState, False)           
        
        return (True, currentState, True) 
        
        
    def verifyRestrictions(self, elementReceivingAction, elementFace, currentState):   
        stringRelations, postRelations = currentState
        
        #Verify if the post is crossing the ring, if it's, then the sphere can't cross the ring.
        for relation in postRelations:
            if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle):
                return False

        return True 
        
    
    def verifyNumberOfOcurrences(self, elementReceivingAction, currentState, newState):
        #stringRelations, postRelations = currentState
        newStringRelations, postRelations = newState
        
        '''
        numberOfOcurrencesBeforeAction = 0
        for relation in stringRelations:
            if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle):
                numberOfOcurrencesBeforeAction += 1
        '''
        numberOfOcurrencesAfterAction = 0
        for relation in newStringRelations:
            if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle):
                numberOfOcurrencesAfterAction += 1
        
        # The string is not infinity.
        if(numberOfOcurrencesAfterAction > enviromentDefinition.numberOfSphereCrossingRing):
            return False
        
        
        return True