% SORTS -------------------------------------------------------

object(X) :- long(X); regular(X); hole(X).
sign(+). sign(-).
opposite(X^ +, X^ -). opposite(X^ -,X^ +).
tip(X^ V) :- long(X), sign(V).
face(H^ V) :- hole(H), sign(V).
node(N) :- tip(N); regular(N); hole(N).

% Actions different from pass(B,F)
:- dynamic allow_cutting/0.
action(cut(T)):-tip(T),allow_cutting.

% BUNDLES: clusters of linked objects --------------------------

add_bundles(S,S1) :-
	findall([X,Y],member(holds(linked(X,Y),true),S),Links), get_bundles(Links,Bs),
	findall(holds(bundle(B),true),member(B,Bs),Fs1),
	findall(holds(bundle([N]),true),(node(N),\+ (member(B1,Bs),member(N,B1))), Fs2),
	merge_set(Fs1,Fs2,Fs),
	merge_set(S,Fs,S1).
	
get_bundles([],[]):-!.
get_bundles([[X,Y]|Links],[B|Bs]):-
	sort([X,Y],B0),get_bundle(B0,Links,B,Links2),get_bundles(Links2,Bs).

get_bundle(B,Links,Bn,Linksn):-
	remove([X,Y],Links,Links2),
	( member(X,B), member(Y,B), !, get_bundle(B,Links2,Bn,Linksn)
	; member(X,B),\+ member(Y,B), !,merge_set([Y],B,B2), get_bundle(B2,Links2,Bn,Linksn)
	; member(Y,B),\+ member(X,B), !,merge_set([X],B,B2), get_bundle(B2,Links2,Bn,Linksn)
	). 

get_bundle(B,Links,B,Links).

remove(X,L,L2):-append(Pre,[X|Suf],L),append(Pre,Suf,L2).

% UNFEASIBILITY ------------------------------------------------
:- dynamic imposs/2.
crossedBy(H,Xs,S):- findall(X,crosses(X,H,S),Xs).
crosses(X,H,S):-member(holds(chain(X),L),S),member(H^ _,L),!.

imposs(pass(B,_T),S):- \+ member(holds(bundle(B),true),S).
imposs(pass(B,H^ _V),S):- member(X,B), crossedBy(H,A,S), cannotPass(X,H,A).
imposs(cut(T), S) :- \+ haslink(T,S).

%imposs(pass(B,_),S) :- member(H,B),member(holds(chain(X),L),S),member(H^ _, L),rigid(X).

haslink(T,S) :- member(holds(linked(T,_),true),S),!.

% EFFECTS -----------------------------------------------------

replace(S0,holds(X,V),S1):- append(Pre,[holds(X,_)|Suf],S0), append(Pre,[holds(X,V)|Suf],S1).

transition(S,A,S2):-findall(F,(member(F,S),\+ F=holds(bundle(_),_)),Fs),
					trans(Fs,A,S1),
					add_bundles(S1,S2).

trans(S0, cut(X^ V), S1):- 
    !, remove(holds(linked(X^ V,_Y),true),S0,S1).

trans(S0, pass([],_F), S0)      :- !.
trans(S0, pass([X^ V|B],F), S1) :- !, trans(S0,pass_t(X^ V,F),S), trans(S,pass(B,F),S1).
trans(S0, pass([H|B],F), S1)    :- !, trans(S0,pass_h(H,F),S), trans(S,pass(B,F),S1).
	
trans(S0, pass_t(X^ V,F), S1) :-
    !,member(holds(chain(X),A),S0), passtip(X^V,F,A,B),
    replace(S0,holds(chain(X),B),S1).

trans([], pass_h(_,_), []):-!.
trans([holds(chain(X),A)|S0], pass_h(H,P), [holds(chain(X),B)|S1]):-
    !, passhole(H,P,A,B), trans(S0,pass_h(H,P),S1).
trans([Fact|S0], pass_h(H,P), [Fact|S1]):-
    !, trans(S0,pass_h(H,P),S1).

passtip(X^ +,P,A,G) :- !, append(B,[Y,X^ +],A),
    (opposite(P,Y),!, append(B,[X^ +],G)                         % (PR)
    ; append(B,[Y,P,X^ +],G)).                                   % (PL)
passtip(X^ -,P,[X^ -,P|B],[X^ -|B]) :- !.                        % (NL)
passtip(X^ -,P,[X^ -,Y|B],[X^ -,P1,Y|B]) :- !,opposite(P,P1).    % (NR)

passhole(_,_, [X],[X]) :- !.
passhole(H,P, [X,G^V|B],[X|D]) :- G\=H,!,passhole(H,P, [G^V|B],D). 
passhole(H,P, [X,H^V,Y|B],[X,P,H^V,P1|G]) :- 
    opposite(P,P1), X \= P1, Y \= P, !, passhole(H,P, [Y|B],G).   % (1R)
passhole(H,P, [P1,H^V,P|B], [H^V|G]) :- 
    opposite(P,P1),!,passhole(H,P, [P|B],L), L=[P|G].             % (1L)
passhole(H,P, [X,H^V,P|B],[X,P,H^V|G]) :- 
    opposite(P,P1), X \= P1, !, passhole(H,P, [P|B],L), L=[P|G].  % (2R)
passhole(H,P, [P1,H^V,Y|B], [H^V,P1|G]) :-                        % (2L)
    opposite(P,P1),Y \= P,!,passhole(H,P, [Y|B],G).

% TEMPORAL PROJECTION -----------------------------------------

execute(S,[]) :- !,	write_state(S).
execute(S,[A|As]) :- !,	
	write_state(S),nl,write(' EXECUTEDACTION: '),write(A),nl,
	( imposs(A,S),!,write(imposs(A)), write(' IMPOSSIBLEACTION: '), write_state(S), nl,fail
	; transition(S,A,S1),execute(S1,As)
	).

% PLANNING: DEPTH-FIRST ITERATIVE DEEPENING -------------------
find_plan(MinDepth,MaxDepth):-
	initial(S0),add_bundles(S0,S1), % Initial sets S0 to the relation that was defined on the function Initial of the file quebraCabeSimples.pl
	iterate_depth(MinDepth,MaxDepth,S1,Sol),write_solution(Sol).
  
iterate_depth(N,Limit,_,_):-N>Limit,!,fail.
iterate_depth(N,Limit,S0,Sol):-
    write('Trying depth '),write(N),nl,
    get_plan(N,[S0],[],Sol)
  ; M is N+1, iterate_depth(M,Limit,S0,Sol).
  
get_plan(_N,[S0|Ss],Plan,([S0|Ss],Plan)):- is_goal(S0),nl.
get_plan(N,_,P,_):-length(P,M),M>=N,!,fail.
get_plan(N,[S0|Ss],Plan0,Sol):- 
	% Get some action
	( member(holds(bundle(B),true),S0), face(F),   % take a current bundle and a face
	  A=pass(B,F),
	  \+ undo_previous_action(A,Plan0)
	; action(A), A \= pass(_,_)
	),

  \+ imposs(A,S0),
  transition(S0,A,S1),
  \+ violated_constraint(S1), 
  get_plan(N,[S1,S0|Ss],[A|Plan0],Sol).

undo_previous_action(pass(B,F),[pass(B,F1)|_]):-opposite(F,F1),!.

% Reject states with two repeated crossings... h+,h+
violated_constraint(S) :-
  member(holds(chain(_Y),L),S), append(_Pre,[X,X|_Suf],L).

getsubset([],[]):-!.
getsubset([_X|Xs],Ys):-getsubset(Xs,Ys).
getsubset([X|Xs],[X|Ys]):-getsubset(Xs,Ys).

write_solution((T,P)):-
  reverse(T,T1),reverse(P,P1),
  write_states(T1,P1).
write_states([],_):-!.
write_states([S],[]):-write_state(S).
write_states([S|Ss],[A|As]):-
  write_state(S), write_action(A),write_states(Ss,As).
write_state(S):-member(holds(chain(X),L),S), write(S), write(' NEWCHAIN '), write(chain(X)=L),nl,fail. % Print how the current chains are.
write_state(_):-nl.
write_action(A):-write('EXECUTEDACTION: '),write(A),nl.
