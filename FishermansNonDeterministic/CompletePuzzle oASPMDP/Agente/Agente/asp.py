import asp2py
import py2asp
import mdp
from random import random

from exp import RUN_ASP, DETERMINISTIC


################################################################################
def updateASPfiles(qTable, pTable, currentState, action, newState,
        reward, minimumReward, maximumReward,
        goalStates, forbiddenStates, forbiddenActionState, forbiddenActionPerformed, forbiddenActions, allActions, nonDeterministicExecuted, actionsByState):

    somethingChanged = False
    removeForbiddenAction = False
    # update restriction list

    # forbidden state-action pairs
    ## criteria: transition to the same state
    if DETERMINISTIC or not nonDeterministicExecuted:
        if (currentState == newState or reward == minimumReward) and [currentState, action] not in forbiddenActionState:
            forbiddenActionState.append([currentState, action])
            
            if RUN_ASP:
                updateRestrictStates = False
                actionToDelete = []
                actionToDelete.append(action)
                actionsByState[currentState] = set(actionsByState[currentState]) - set(actionToDelete)
                
                if(len(actionsByState[currentState]) <= 0):
                    forbiddenStates.append(currentState)
                    updateRestrictStates = True
                if(len(actionsByState[newState]) <= 0):
                    forbiddenStates.append(newState)
                    updateRestrictStates = True    
                    
                if(updateRestrictStates): #If there is a state where all actions were removed
                    py2asp.restrictStates(forbiddenStates)
                
                py2asp.restrictSApairs(forbiddenActionState, currentState)
            
            somethingChanged = True
            removeForbiddenAction = True

    # forbidden actions
    # criteria: Received from the environment
    if(forbiddenActionPerformed):
        if action not in forbiddenActions:
            forbiddenActions.append(action)
            
            if RUN_ASP:
                py2asp.restrictActions(forbiddenActions)
            
            somethingChanged = True
        removeForbiddenAction = True   

    # forbidden states
    ## criteria: minimum reward
    #this code is commented because I wanted to test something first
    '''if reward == minimumReward and newState not in forbiddenStates or True:
        forbiddenStates.append(newState)
        if RUN_ASP:
            py2asp.restrictStates(forbiddenStates)
        somethingChanged = True
    '''
            
    # save goal state
    ## criteria: maximum reward
    if reward == maximumReward and newState not in goalStates:
        goalStates.append(newState)
        
        if RUN_ASP:
            py2asp.saveGoalStates(goalStates)
        
        somethingChanged = True

    # update Q and P functions
    if currentState not in pTable:
        pTable[currentState] = dict()
        somethingChanged = True

    if currentState not in qTable:
        somethingChanged = True
        if(RUN_ASP):#This update is done faster then calling ASP every time. But it's the same result
            qTable[currentState] = dict()
            for actionAux in actionsByState[currentState]:
                qTable[currentState][actionAux] = random()
        else:
            qTable[currentState] = dict()
            for actionAux in allActions:
                qTable[currentState][actionAux] = random() #0

    if action not in pTable[currentState]:
        pTable[currentState][action] = list()
        somethingChanged = True

    if(not RUN_ASP):
        if action not in qTable[currentState]:
            qTable[currentState][action] = random() #0
            somethingChanged = True

    if newState not in pTable[currentState][action]:
        pTable[currentState][action].append(newState)
        somethingChanged = True

    
    #This update already happens in the agent code. It's doing the same thing twice
    if somethingChanged:
        if RUN_ASP:
            #Uncomment this code if using ASP (slower than list) to update the QTable.
            '''listToChange = []
            listToChange.append(currentState)
            newQTableFromASP = genNewQ(pTable, listToChange)
            qTable = mdp.updateQwithASP(qTable, newQTableFromASP)'''
            if(removeForbiddenAction):
                if(action in qTable[currentState]):
                    del qTable[currentState][action]
            saveStateDesc(pTable, currentState)
    
    
    return somethingChanged


################################################################################
def genNewQ(pTable, changedStates):
    return asp2py.genQfromLP(pTable.keys(), changedStates)


################################################################################
def saveActionList(actions, forbiddenActions):
    py2asp.saveActions(actions)
    py2asp.restrictActions(forbiddenActions)


################################################################################
def saveStateDesc(pTable, state):
    py2asp.psa2lp(pTable, state)


################################################################################
