from exp import DEBUG
from random import random

################################################################################
def findAS(lp='*.lp'):
    from os import popen

    for f in lp.split():
        arg = 'touch ' + f
        popen(arg)

    models = list()

    args = 'clingo ' + lp + ' 0 '
    output = [s for s in popen(args)]

    for i, s in enumerate(output):
        if('Answer' in s):
            models.append(output[i + 1])

    return models


################################################################################
def statesAnSets(state):
    s = 's' + state + '.lp '
    #s += 'actions.lp rActions.lp rStates.lp '
    s += 'actions.lp restrictActions.lp rStates.lp '
    s += 's' + state + 'r.lp '

    return findAS(s)


################################################################################
def genQfromLP(states, changedStates):
    qTable = dict()

    for state in changedStates:
        qTable[state] = dict()
        
        #Clingo call for the state
        asets = statesAnSets(state)

        for aset in asets:
            action = aset.split(" ")
            action.sort()
            action = action[0]
            i = action.find("(") + 1
            e = action.find(")")
            action = action[i:e]
            qTable[state][action] = random() #0.0

    return qTable


################################################################################
