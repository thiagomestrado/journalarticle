import ast
from pyparsing import _MAX_INT

# experiments variables
nenvs = 3
episodes = 6000
trials   = 30
maxSteps = 500

# RL vars
learningRate = 0.2
discount = 0.9 #Favor future actions, rather than recent ones.
initialEpsilon = 0.1
stepToUpdateEpsilon = 250

# Configuration variables
DEBUG = False
RUN_ASP = True
DETERMINISTIC = False
USE_HEURISTIC = False

# Heuristic Variables
heuristicValues = {} #List of heuristic values
heuristicStateMapping = dict()
if(USE_HEURISTIC):
    heuristicFile = open("heuristic", "r")
    stringHeuristicValues = heuristicFile.read()
    heuristicFile.close()
    heuristicValues = ast.literal_eval(stringHeuristicValues)

port = 5060
qDifferenceWindow = 10
graphicValuesWindow = 1
