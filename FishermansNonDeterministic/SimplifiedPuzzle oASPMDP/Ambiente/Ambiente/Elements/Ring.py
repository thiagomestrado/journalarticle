from Elements.Element import *
from enumerations import ElementType
from enviromentDefinition import *
from ElementsRelation import ElementsRelation
import enviromentDefinition
from utilMethods import *
import utilMethods

class Ring(Element):
    def __init__(self, nameOfElement, initialIndex, orderInPuzzle):
        Element.__init__(self, ElementType.RING, orderInPuzzle, False)
        self.name = nameOfElement
        self.initialIndex = initialIndex
        self.orderInPuzzle = orderInPuzzle        
    
    def printElementType(self):
        print("This is an element of type RING")
        
    def executeAction(self, elementReceivingAction, elementFace, currentState):  
        if (elementReceivingAction.elementType == ElementType.POST):
            
            continueExecution = self.verifyRestrictions(elementReceivingAction, elementFace, currentState)
            if(not continueExecution):
                return (True, currentState, False) 
            
            entitiesOfAction = (self, elementReceivingAction, elementFace)

            util = UtilMethods()
            complexPrologResponse = util.executePrologCommand(currentState, entitiesOfAction)
            actionExecuted, newState = complexPrologResponse
            
            if(actionExecuted):
                if(not self.verifyNumberOfOcurrences(elementReceivingAction, currentState, newState)):
                    return (True, currentState, False)                
                
                if(newState != currentState):
                    return (False, newState, False)

            return (True, currentState, False)           
        
        return (True, currentState, True)   
    
    def verifyRestrictions(self, elementReceivingAction, elementFace, currentState):   
        stringRelations, postRelations = currentState
        
        #Verify if the post is crossing the ring. If this is true, then it can't continue
        for relation in postRelations:
            if(relation.elementInChain.orderInPuzzle == self.orderInPuzzle):
                return False
            
        return True
    
    
    def verifyNumberOfOcurrences(self, elementReceivingAction, currentState, newState):
        stringRelations, postRelations = currentState
        newStringRelations, postRelations = newState
        
        '''numberOfOcurrencesBeforeAction = 0
        for relation in stringRelations:
            if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle):
                numberOfOcurrencesBeforeAction += 1
        '''   
        numberOfOcurrencesAfterAction = 0
        for relation in newStringRelations:
            if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle):
                numberOfOcurrencesAfterAction += 1
        
        if(numberOfOcurrencesAfterAction > enviromentDefinition.numberOfStringCrossingPost):
            return False
        
        return True
    
    