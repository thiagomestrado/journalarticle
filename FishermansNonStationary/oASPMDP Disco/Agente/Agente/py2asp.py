################################################################################
from feedparser import ACCEPTABLE_URI_SCHEMES
def dict2asp(pTable, state):

    aspCode = ''
    for action in pTable[state]:
        newStates = list()
        for ns in pTable[state][action]:
            newStates.append(ns)
        aspCode += descState(newStates, action)

    return aspCode


################################################################################
def descState(states, action):
    choiceI = '1{ '
    choiceE = ' }1'
    choiceS = '; '

    sInit = 's('
    sSep = ','
    sEnd = ')'

    cond = ' :- '

    actionI = 'a('
    actionE = ')'

    ruleE = '.\n'


    # generate states list
    s = ''
    size = len(states)
    for i,state in enumerate(states):
        s += sInit + str(state) + sEnd

        # place choices separator
        if(i < size - 1): s += choiceS

    return choiceI + s + choiceE + cond + actionI + action + actionE + ruleE


################################################################################
def restrictStates(states):

    code = ''
    for s in states:
        code += ':- s(' + str(s) + ').\n'

    save('restrictStates.lp', code)


################################################################################
def restrictActions(actions):
    code = ''
    for a in actions:
        code += ':- a(' + str(a) + ').\n'

    save('restrictActions.lp', code)


################################################################################
def restrictSApairs(pairs, state):
    code = ''

    for pair in pairs:
        s = pair[0]
        if s == state:
            a = pair[1]
            code += ':- a(' + str(a) + ').\n'

    name = 's' + s + 'r.lp'
    save(name, code)


################################################################################
def saveStates(states):

    code = ''
    for s in states:
        code += ':- s(' + s + ').\n'

    save('states.lp', code)


################################################################################
def saveActions(actions):
    code = '1{ '

    for a in actions[:-1]:
        code += 'a(' + str(a) + '); '

    code += 'a(' + str(actions[-1]) + ') }1.'

    save('actions.lp', code)

################################################################################
def saveGoalStates(states):

    code = ''
    for s in states:
        code += ':- s(' + s + ').\n'

    save('goalStates.lp', code)


################################################################################
def save(fname, text):
    lprogram = open(fname, 'w')
    lprogram.write(text)
    lprogram.close()


################################################################################
def psa2lp(pTable, currentState):
    fileName = 's' + currentState + '.lp'
    code = dict2asp(pTable, currentState)
    save(fileName, code)


################################################################################