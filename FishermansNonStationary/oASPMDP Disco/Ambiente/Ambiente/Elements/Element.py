from enumerations import *
import enviromentDefinition
from enviromentDefinition import *

class Element:
    
    def __init__(self, elementType, orderInPuzzle, containHole):
        self.elementType = elementType
        self.orderInPuzzle = orderInPuzzle
        self.containHole = containHole
    
    def printElementType(self):
        print("The type of this element is: " + str(self.elementType))
    
    def executeAction(self, elementReceivingAction, ElementFace, currentState):
        print("It's not possible to execute an action in the base class!")
        return False
     
    def determineActionToExecute(self, elementReceivingAction, elementFace, currentState, nonStationary):
        return self.executeAction(elementReceivingAction, elementFace, currentState, nonStationary)    

            
