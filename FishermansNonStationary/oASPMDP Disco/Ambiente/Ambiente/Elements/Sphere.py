from Elements.Element import *
from enumerations import ElementType
from enviromentDefinition import *
import enviromentDefinition
from utilMethods import *
import utilMethods

class Sphere(Element):
    def __init__(self, nameOfElement, initialIndex, orderInPuzzle):
        Element.__init__(self, ElementType.SPHERE, orderInPuzzle, False)
        self.name = nameOfElement
        self.initialIndex = initialIndex
        self.orderInPuzzle = orderInPuzzle        
    
    def printElementType(self):
        print("This is an element of type SPHERE")
        
    def executeAction(self, elementReceivingAction, elementFace, currentState, nonStationary):  
        if (elementReceivingAction.elementType == ElementType.RING):
            
            entitiesOfAction = (self, elementReceivingAction, elementFace)

            continueExecution = self.verifyRestrictions(elementReceivingAction, elementFace, currentState)
            if(not continueExecution):
                return (True, currentState, False) 

            util = UtilMethods()
            complexPrologResponse = util.executePrologCommand(currentState, entitiesOfAction)
            actionExecuted, newState = complexPrologResponse
            
            if(actionExecuted):
                if(not self.verifyNumberOfOcurrences(elementReceivingAction, currentState, newState)):
                    return (True, currentState, False)
                
                if(self.identifyKnot(newState)):
                    return (True, currentState, False)  
                
                if(newState != currentState):
                    return (False, newState, False)

            return (True, currentState, False)           
        
        return (True, currentState, True) 
        
        
    def verifyRestrictions(self, elementReceivingAction, elementFace, currentState):   
        stringRelations, postRelations = currentState
        
        #Verify if the post is crossing the ring, if it's, then the sphere can't cross the ring.
        if(elementReceivingAction.orderInPuzzle == enviromentDefinition.ring.orderInPuzzle):
            for relation in postRelations:
                if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle):
                    return False

        return True 
        
    
    def verifyNumberOfOcurrences(self, elementReceivingAction, currentState, newState):
        #stringRelations, postRelations = currentState
        newStringRelations, postRelations = newState
        
        '''
        numberOfOcurrencesBeforeAction = 0
        for relation in stringRelations:
            if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle):
                numberOfOcurrencesBeforeAction += 1
        '''
        numberOfOcurrencesAfterAction = 0
        for relation in newStringRelations:
            if(relation.elementInChain.orderInPuzzle == enviromentDefinition.ring.orderInPuzzle):
                numberOfOcurrencesAfterAction += 1
        
        # The string is not infinity.
        if(numberOfOcurrencesAfterAction > enviromentDefinition.numberOfSphereCrossingRing):
            return False
        
        
        return True
    
    
    def identifyKnot(self, newState):
        stringRelations, postRelations = newState
        
        numberOfKnots = 0
        
        currentElementToEvaluate = None
        for relation in stringRelations:
            #Only consider the post elements, because it's with them that knots are created.
            if(relation.elementInChain.orderInPuzzle == enviromentDefinition.post.orderInPuzzle):
                if(currentElementToEvaluate is None): #If it's the first post element identified.
                    currentElementToEvaluate = relation
                else:
                    #If it's another post on the sequence, then it doesn't create a knot.
                    if(relation.elementInChain.orderInPuzzle != currentElementToEvaluate.elementInChain.orderInPuzzle):
                        currentElementToEvaluate = relation
                    else:
                        #If it's the same post on the sequence but with different face, then it doesn't create a knot.
                        if(relation.face != currentElementToEvaluate.face):
                            currentElementToEvaluate = relation
                        else: #Knot identified!!!!
                            #return True
                            numberOfKnots += 1
                            currentElementToEvaluate = relation

        if(numberOfKnots > enviromentDefinition.allowedNumberOfKnots):
            return True
        
        #If a knot is NOT identified
        return False