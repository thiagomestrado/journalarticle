% defining the elements of the Fishermans Folly Puzzle -----
% Seven elements: Post, String, Ring, Disk1, Disk2, Sphere1 and Sphere2

% Long elements
long(str).
long(post).

% Disks
disk(disk1). 
disk(disk2).

% Spheres
sphere(sphere1). 
sphere(sphere2).

% Elements with holes
hole(ring).
hole(posth).  
hole(S):-sphere(S).

% Regular Elements
regular(D) :- disk(D).
regular(base).


% Actions that can never be executed.
cannotPass(D,S,[])        :-disk(D),sphere(S).
#cannotPass(D,ring,[])     :-disk(D).
cannotPass(D, ring, [post]) :-disk(D).

cannotPass(base,ring,[]).
cannotPass(base,posth,[]).
cannotPass(base,S,[])	  :- sphere(S).

cannotPass(post,S,[])     :-sphere(S).
cannotPass(post,posth,[]).
cannotPass(posth,S,[])    :-sphere(S).

cannotPass(S,posth,[])    :-sphere(S).
cannotPass(S,S1,[])       :-sphere(S), sphere(S1).
cannotPass(S,ring,[post]) :-sphere(S).

cannotPass(ring,S,[])     :-sphere(S).
cannotPass(ring,D,[])     :-disk(D).
cannotPass(ring, str, []).
cannotPass(ring, post, []).

% Actions that cannot be executed depending on the conditions of the puzzle.
cannotPass(H, H,_)		:- hole(H).
cannotPass(X, H, Z)		:- Z\=[],getsubset(Z,Y),Y\=Z,cannotPass(X,H,Y),!.
cannotPass(L^ V,H,Z)		:- long(L), cannotPass(L,H,Z), \+ canPass(L^ V, H,Z).

% Actions that can be executed. If it passes trhough all options above, then the action can be executed.
canPass(a, a, []).


% INITIAL STATE ------------------------------------------------

initial( [
  holds(linked(str^ -, disk1), true),
  holds(linked(str^ +, disk2), true),
  holds(linked(post^ -, base), true),
  holds(linked(post^ +, posth), true),
  holds(chain(str),[str^ -, sphere1^ +, posth^ +, sphere2^ +, str^ +]),
  holds(chain(post),[post^ -, ring^ +, post^ +])
]).

% If it is the first interaction, then it just has to perform the action on the default initial state of the puzzle.
start(ACTION) :- 
	initial(S0),
	add_bundles(S0,S1),
	execute(S1, ACTION).

% If it is not the first interaction, then it has to send the current state of the puzzle and the action to be executed.
start(STATE, ACTION) :-  
	S0 = STATE,	
	add_bundles(S0,S1),
	execute(S1, ACTION).

% GOAL ---------------------------------------------------------

is_goal(S) :- crossedBy(ring,[],S).
