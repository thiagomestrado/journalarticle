from enumerations import ElementFace
from enumerations import ElementType
from subprocess import call, Popen
import subprocess
from asyncio.subprocess import PIPE
from enviromentDefinition import *
import enviromentDefinition
from chainRelations import *
import chainRelations

class UtilMethods:    
    def getElementsLinks(self):
        listOfLinkedElements = []
        
        #Elements linked to the String
        listOfLinkedElements.append("holds(linked(str^ -, disk1), true)")
        listOfLinkedElements.append("holds(linked(str^ +, disk2), true)")
        
        #Elements linked to the Post
        listOfLinkedElements.append("holds(linked(post^ -, base), true)")
        listOfLinkedElements.append("holds(linked(post^ +, posth), true)")
        
        return listOfLinkedElements
    
    
##############################################################################################################        
    def getSymbolFromFaceEnum(self, face):
        if(face == ElementFace.POSITIVE):
            return "+"
        else:
            return "-" 
    
    
##############################################################################################################        
    def getSpecificChainFromComplexState(self, complexStateChain, hostChainElementType):
        nameOfChain = "str" if hostChainElementType == ElementType.MAINSTRING else "post"
        chainCommand = "holds(chain(" + nameOfChain +"),["
        
        for relation in complexStateChain:
            chainCommand += self.getElementReceivingActionHasHoleChain(relation.elementInChain, hostChainElementType) + "^ " + self.getSymbolFromFaceEnum(relation.face) + ","
        
        indexOfLastCharacter = len(chainCommand) - 1
        chainCommand = chainCommand[:indexOfLastCharacter] + '])'
        
        return chainCommand
    
    
##############################################################################################################    
    def getChainsFromComplexState(self, complexState):
        stringChain, postChain = complexState
        
        stringChainCommand = self.getSpecificChainFromComplexState(stringChain, ElementType.MAINSTRING)
        postChainCommand = self.getSpecificChainFromComplexState(postChain, ElementType.POST)
        
        return (stringChainCommand, postChainCommand)       
    
    
##############################################################################################################        
    def getElementReceivingActionHasHole(self, element):
        if (element.containHole):
            return element.name + "h"
        else:
            return element.name
        
        
##############################################################################################################        
    def getElementReceivingActionHasHoleChain(self, element, hostChainElementType):
        if (element.containHole and element.elementType != hostChainElementType):
            return element.name + "h"
        else:
            return element.name
        
        
##############################################################################################################        
    def getHostElementToPerformAction(self, element):
        if(element.elementType == ElementType.POST):
            return "posth, post^ +" #Actions with post can only be executed like this
        elif(element.elementType == ElementType.DISK):
            if(element.name == "disk1"):
                return "disk1, str^ -" #Actions with disk can only be executed like this 
            else:
                return "disk2, str^ +"
        else:
            return element.name
    
    
##############################################################################################################        
    def mountPrologCommand(self, currentComplexState, entitiesOfAction):
        stateInProlog = "STATE = ["
        
        #Mounting the part of the command that is related to the state and links.
        listOfLinkedElements = self.getElementsLinks()
        for link in listOfLinkedElements:
            stateInProlog += link + ","
            
        #Mounting the part of the command that is related to the chains.
        listOfChains = self.getChainsFromComplexState(currentComplexState)
        for chain in listOfChains:
            stateInProlog += chain + ","
            
        indexOfLastCharacter = len(stateInProlog) - 1
        stateInProlog = stateInProlog[:indexOfLastCharacter] + '],'
        
        #Creating the command for Action
        hostElement, elementReceivingAction, elementFace = entitiesOfAction
        
        actionToExecuteInProlog = self.getHostElementToPerformAction(hostElement)
        elementReceivingAction = self.getElementReceivingActionHasHole(elementReceivingAction) + "^ " + self.getSymbolFromFaceEnum(elementFace)
                                 
        actionInProlog = "ACTION = [pass([" + actionToExecuteInProlog + "], " + elementReceivingAction + ")],"
        #print("Acao: " + actionInProlog)
        
        finalPredicate = "start(STATE, ACTION)."
        finalCommand = stateInProlog + actionInProlog + finalPredicate
            
        return finalCommand
    

##############################################################################################################        
    def getElementFromRepresentationInProlog(self, elementInProlog):
        face = ElementFace.POSITIVE
        
        if(elementInProlog.find("-") > -1):
            face = ElementFace.NEGATIVE
            
        element = enviromentDefinition.post
        if(elementInProlog.find("str") > -1):
            element = enviromentDefinition.mainString
        elif (elementInProlog.find("ring") > -1):
            element = enviromentDefinition.ring
        elif (elementInProlog.find("disk1") > -1):
            element = enviromentDefinition.disk1
        elif (elementInProlog.find("disk2") > -1):
            element = enviromentDefinition.disk2
        elif (elementInProlog.find("sphere1") > -1):
            element = enviromentDefinition.sphere1
        elif (elementInProlog.find("sphere2") > -1):
            element = enviromentDefinition.sphere2
        
        return (element, face)
    
    
##############################################################################################################        
    def getComplexChainFromProlog(self, hostElement, chainElements):    
        complexChain = list()
        
        for element in chainElements:
            elementInChain, face = self.getElementFromRepresentationInProlog(element)
            relationToAdd = ChainRelations(hostElement, elementInChain, face)
            complexChain.append(relationToAdd)
        
        return complexChain
        

##############################################################################################################        
    def getNewStateFromProlog(self, prologResponse):
        impossibleActionExecuted = "IMPOSSIBLEACTION"
        #print(prologResponse)
        indexOfImpossible = prologResponse.find(impossibleActionExecuted)
        if(indexOfImpossible > -1):
            return (False, False) #If it's impossible to execute, then it returns false to the executed with success.
        
        beginUtilResponse = "EXECUTEDACTION"
        endOfUtilSubString = "STATE"
        chainBegin = "NEWCHAIN"
        endOfChain = "\\n"
        indexBegin = prologResponse.find(beginUtilResponse)
        indexEnd = prologResponse.find(endOfUtilSubString)
        
        utilPartOfPrologResponse = prologResponse[indexBegin:indexEnd]
        
        #Get a String with the chain that is responsible for keeping the elements that relate to the main string
        indexBeginOfStringChain = utilPartOfPrologResponse.find(chainBegin)
        utilPartOfPrologResponse = utilPartOfPrologResponse[indexBeginOfStringChain:]
        stringChain = utilPartOfPrologResponse
        indexOfEndOfStringChain = stringChain.find(endOfChain)
        stringChain = stringChain[0:indexOfEndOfStringChain]
        
        #Get a String with the chain that is responsible for keeping the elements that relate to the Post
        postChain = utilPartOfPrologResponse[indexOfEndOfStringChain:]
        indexOfBeginOfPostChain = postChain.find(chainBegin)
        postChain = postChain[indexOfBeginOfPostChain:]
        
        #Removing unecessary characters from the chain representation.
        stringChain = stringChain[stringChain.find("[")+1:len(stringChain) - 1]
        postChain = postChain[postChain.find("[")+1:len(postChain) - 1]
        
        #Spliting the chains into each element.
        stringChainElements = stringChain.split(",")
        postChainElements = postChain.split(",")
        
        complexStringChain = self.getComplexChainFromProlog(enviromentDefinition.mainString, stringChainElements)
        complexPostChain = self.getComplexChainFromProlog(enviromentDefinition.post, postChainElements)
        
        complexNewState = (complexStringChain.copy(), complexPostChain.copy())
        
        
        return (True, complexNewState)
    
##############################################################################################################        
    def executePrologCommand(self, currentComplexState, entitiesOfAction):
        namesOfPrologFiles = "['" + enviromentDefinition.puzzlePlannerProlog + "', '" + enviromentDefinition.puzzleDefinitionProlog + "'].  "
        
        commandToExecute = namesOfPrologFiles
        commandToExecute += self.mountPrologCommand(currentComplexState, entitiesOfAction) + " ." #this last point(.) is to end the prolog call on the terminal.
        
        process = Popen(["swipl"], shell=False, stdout=PIPE, stdin=PIPE)
        prologResponse = process.communicate(commandToExecute.encode())
        
        '''
        text_file = open("1TesteCommandos.txt", "a")
        text_file.write("\n\n\n")
        text_file.write(str(commandToExecute))
        text_file.write("\n\n\n")
        text_file.close()
        '''
        
        return self.getNewStateFromProlog(str(prologResponse[0]))
    
    