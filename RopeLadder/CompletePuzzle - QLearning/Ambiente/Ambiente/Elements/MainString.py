from Elements.Element import *
from enumerations import ElementType
from enviromentDefinition import *
import enviromentDefinition
from utilMethods import *
import utilMethods

class MainString(Element):
    def __init__(self, nameOfElement, initialIndex, orderInPuzzle):
        Element.__init__(self, ElementType.MAINSTRING, orderInPuzzle, False)
        self.name = nameOfElement
        self.initialIndex = initialIndex
        self.orderInPuzzle = orderInPuzzle
    
    def printElementType(self):
        print("This is an element of type STRING")
        
    def executeAction(self, elementReceivingAction, elementFace, currentState):
        #The String is manipulated with its tips that are fixed to Disk1 and Disk2
        return (True, currentState, True)
    
    
    