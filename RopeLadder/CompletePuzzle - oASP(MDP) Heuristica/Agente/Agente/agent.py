
# for printing Q and P tables
import pprint
from debian.changelog import change
from fnmatch import fnmatch
pp = pprint.PrettyPrinter(depth=100)

# random methods for action selection and state initialization
from random import random
from random import choice

# learning variables
from exp import *
import exp

# online methods of oASP(MDP)
import online

# ASP methods of oASP(MDP)
import asp

# MDP methods of oASP(MDP)
import mdp

# episode variables
tSteps = 0   # total steps in a episode
actions = [] # actions list

maximumReward = 0     # maximum value for reward
minimumReward = 0     # minimum value for reward

'''forbiddenStates = []  # list of forbidden states
forbiddenActions = []  # list of forbidden actions
forbiddenActionState = [] # list of forbidden state-action pairs
goalState = []  # list of goal states
actionsByState = dict()'''

################################################################################
def saveData(qs, diffQ, steps, ret, currentQTable, timeToExecute, numberOfStateActionPairs, trial):
    from pickle import dump
    from os import popen

    trial = trial + 5

    if RUN_ASP:
    	popen('find . -maxdepth 1 -name "*.lp" -print0 | xargs -0 rm')
    	time.sleep(60)

    fname = 'diffQ-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(diffQ, f)

    fname = 'steps-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(steps, f)

    fname = 'ret-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(ret, f)

    fname = 'qsa-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(qs, f)

    fname = 'stateActionPair-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(numberOfStateActionPairs, f) 
        
    fname = 'timeToExecute-' + str(trial) + '.pck'
    with open(fname,'wb') as f:
        dump(timeToExecute, f)
        
    fname = 'output-' + str(trial) + '.txt'
    text_file = open(fname, "w")
    text_file.write("*"*30)
    text_file.write("\n\nQ(s,a)")
    text_file.write(str(currentQTable))
    text_file.write("*"*30)
    text_file.write("\n\n\n Number of States: " + str(len(currentQTable)))
    text_file.close()
    print("Number of States: " + str(len(currentQTable)))
    

################################################################################
def episode(qTable, pTable, updatedEpsilon, forbiddenStates, forbiddenActions, forbiddenActionState, goalState, actionsByState):
#def episode(qTable, pTable, updatedEpsilon):

    global tSteps
    global minimumReward, maximumReward, actions


    # get connection to environment
    socket = online.initSocket(port)

    # receive env information
    maximumReward, minimumReward, actions = online.getConf(socket)

    # save actions for ASP calls
    if RUN_ASP:
        asp.saveActionList(actions, forbiddenActions)

    # agent's initial configuration
    newState, end = online.initState(socket)

    # variables to save
    step = 0
    returnReward = 0

    somethingChanged = False
    changedStates = []

    counter = 0
    
    currentHeuristicState = "n2p6p0p1p7p2bn9p3p9" #Initial state of the simple Rope Ladder
    
    # here is the oASP(MDP)
    while not end:

        ### ONLINE
        # update state
        currentState = newState
        
        
        # choose action
        actionToPerform = mdp.chooseAction(qTable, actions, currentState, choice, forbiddenActions, forbiddenActionState, updatedEpsilon, currentHeuristicState, actionsByState)


        # perform action in the environment
        online.sendMessage(currentState, actionToPerform, socket)

        # receive information from environment
        newState, reward, end, forbiddenActionPerformed = online.receiveMessage(socket)

        ### END ONLINE

        # UPDATE Q AND P FUNCTIONS AND ASP FILES
        changes = asp.updateASPfiles(qTable, pTable,
                currentState, actionToPerform, newState,
                reward, minimumReward, maximumReward,
                goalState, forbiddenStates, forbiddenActionState, forbiddenActionPerformed, forbiddenActions, actions, actionsByState)
        somethingChanged = somethingChanged or changes
        # END UPDATE

        if(newState in forbiddenStates):
            reward = minimumReward
            end = True
            step = maxSteps - 1

        if(actionToPerform in qTable[currentState]):
            qTable[currentState][actionToPerform] = mdp.updateQvalue(qTable, currentState, 
                                                                     actionToPerform, reward, newState)

        
        heuristicAction = int(actionToPerform)
        if(currentHeuristicState in heuristicStateMapping and
           heuristicAction in heuristicStateMapping[currentHeuristicState]):
            currentHeuristicState = heuristicStateMapping[currentHeuristicState][heuristicAction]
        else:
            currentHeuristicState = ""
        
        # variables to send
        step += 1
        returnReward += reward 
        if(reward == 1000):
            print("OBJETIVO!!!")

        tSteps += 1
        if step == maxSteps:
            end = True
            if(reward != maximumReward):
                reward = minimumReward
        ### END MDP

        ### ASP CALL
        if RUN_ASP:
            if(changes):
                if(currentState not in changedStates):
                    changedStates.append(currentState)
            #if changes:
                #asp.saveStateDesc(pTable, currentState)
                #if(currentState not in changedStates):
                    #changedStates.append(currentState)
        ### END ASP CALL

        #if DEBUG: input("Press anykey to continue")

        counter = counter + 1
    online.sendEndEpisode(socket)

    if somethingChanged:
        if RUN_ASP:
            newQTableFromASP = asp.genNewQ(pTable, changedStates)
            qTable = mdp.updateQwithASP(qTable, newQTableFromASP)
        
        #if DEBUG: pp.pprint(newQTableFromASP)
        

    return qTable, pTable, step, returnReward


#Return a new value for Epsilon.
def updateEpsilon(currentEpisode, currentEpsilon):
    if((currentEpisode % exp.stepToUpdateEpsilon) == 0 and currentEpisode != 0):
        currentEpsilon = round(currentEpsilon, 2)
        return (currentEpsilon - 0.01)

    return currentEpsilon

def getNumberOfStateActionPair(currentQTable):
    numberOfStateActionPair = 0
    for state in currentQTable:
        numberOfStateActionPair += len(currentQTable[state])
        
    return numberOfStateActionPair

################################################################################
def experiment():
    
    for trial in range(trials):
        print("Trial: %s" % int(trial+1))
        pTable = dict()
        qTable = dict()
        trialStartTime = time.time()
        
        # variables to be saved
        qTablesList = list()
        steps = list()
        returns = list()
        diffQ = list()
        oldQTable = qTable.copy()
        numberOfStateActionPairs = list()

        #Variables that needs to be reinitialized
        updatedEpsilon = exp.initialEpsilon
        forbiddenStates = []
        forbiddenActions = []
        forbiddenActionState = []
        goalState = []
        actionsByState = dict()

        currentEpisode = 0
        #for currentEpisode in range(nenvs * episodes):
        for currentEpisode in range(episodes):
            print("Episode: %s" % int(currentEpisode))
            qTable, pTable, step, reward = episode(qTable, pTable, updatedEpsilon, forbiddenStates, forbiddenActions, forbiddenActionState, goalState, actionsByState)
            #qTable, pTable, step, reward = episode(qTable, pTable, updatedEpsilon)

            #Values that are going to be used to build graphs and make comparisons.
            if((currentEpisode+1) % qDifferenceWindow == 0):
                differenceBetweenQs = mdp.rmsd(oldQTable, qTable)
                oldQTable = qTable.copy()
                diffQ.append(differenceBetweenQs)
                
            steps.append(step)
            returns.append(reward)
            qTablesList.append(len(qTable)) #To get the number of states in the Q-Table in the current time step.
            numberOfStateActionPairs.append(getNumberOfStateActionPair(qTable))
                
            if(currentEpisode >= 4000):
                updatedEpsilon = updateEpsilon(currentEpisode, updatedEpsilon)
                
            print("Number of States: " + str(len(qTable)))
            print("Steps: " + str(step))

        timeToExecute = time.time() - trialStartTime
        saveData(qTablesList, diffQ, steps, returns, qTable, timeToExecute, numberOfStateActionPairs, trial+1)
        
    print("\n\n")
    #print("Tabela Final " + str(qTable))


################################################################################
import time
start_time = time.time()
experiment()
print("--- %s Horas ---" % ((time.time() - start_time) / 3600))

