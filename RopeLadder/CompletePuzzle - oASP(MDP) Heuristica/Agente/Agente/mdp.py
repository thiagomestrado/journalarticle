from exp import RUN_ASP, USE_HEURISTIC, heuristicValues, heuristicStateMapping
from exp import learningRate, discount, initialEpsilon
from random import random

################################################################################
def updateQvalue(qTable, state, action, reward, newState):

    stateActionQ = qTable[state][action]
    
    if newState not in qTable:
        newStateNewActionQ = 0
    elif not qTable[newState]:
        newStateNewActionQ = 0
    else:
        newStateNewActionQ = qTable[newState][maxAToUpdate(qTable, newState)]

    updatedQ = stateActionQ + learningRate * (reward + discount * newStateNewActionQ - stateActionQ)

    return updatedQ



################################################################################
def maxAToUpdate(qTable, state):
    import operator
    
    qstate = qTable[state]
    
    return max(qstate.items(), key=operator.itemgetter(1))[0]


################################################################################
def maxA(qTable, state, currentHeuristicState):
    import operator
    
    qstate = qTable[state]

    #If the algorithm is going to use heuristics, then it enters in this loop
    if(USE_HEURISTIC):
        
        actionsInHeuristic = heuristicValues.get(currentHeuristicState)
        
        if(actionsInHeuristic != None):
            biggestActionValue = -10000
            biggestAction = 0
            
            for action in qstate:
                actionHeuristicValue = actionsInHeuristic.get(str(action))
                if(actionHeuristicValue == None):
                    actionHeuristicValue = 0
                
                #Weight of the heurisic. 10%
                actionHeuristicValue = actionHeuristicValue * 0.25
                actionHeuristicValue += qstate[action]
                 
                if(actionHeuristicValue > biggestActionValue):
                    biggestAction = action
                    biggestActionValue = actionHeuristicValue
                    
            for action in actionsInHeuristic:
                actionHeuristicValue = qstate.get(action)
                if(actionHeuristicValue == None):
                    actionHeuristicValue = 0
                actionHeuristicValue += heuristicValues[currentHeuristicState][str(action)]
                
                if(actionHeuristicValue > biggestActionValue):
                    biggestAction = action
                    biggestActionValue = actionHeuristicValue
                
            return biggestAction

    return max(qstate.items(), key=operator.itemgetter(1))[0]


################################################################################
def chooseAction(qTable, actions, state, randomMethod, forbiddenActions, forbiddenActionState, updatedEpsilon, currentHeuristicState, actionsByState):
    # randomMethod is the method for choosing random action
    if RUN_ASP:
        permittedActions = allowedActions(state, actions, forbiddenActions, forbiddenActionState, actionsByState)
    else:
        permittedActions = actions
    
    
    #permittedActions = allowedActions(state, actions, forbiddenActions, forbiddenActionState)
    
    return eGreedy(qTable, state, permittedActions, randomMethod, updatedEpsilon, currentHeuristicState)


################################################################################
def eGreedy(qTable, state, actions, randomMethod, updatedEpsilon, currentHeuristicState):
    randomNumber = random()

    #This part is deciding how to get the action, if it's random or using the method.
    if randomNumber <= updatedEpsilon or state not in qTable or not qTable[state]:
        return randomMethod(actions)
    else:
        return maxA(qTable, state, currentHeuristicState) #Return the action with maximum value for a state.


################################################################################
def allowedActions(state, actions, forbiddenActions, forbiddenActionState, actionsByState):
    
    '''allowed = set(actions) - set(forbiddenActions)
    if(len(allowed) <= 0):
        print("Vazia")
        
    
    if(RUN_ASP):
        #removing actions that are not allowed by state.
        actionsToRemove = []
        for action in allowed:
            pair = [state, action]
            if(pair in forbiddenActionState):
                actionsToRemove.append(action)
        
        for action in actionsToRemove:
            allowed.remove(action)'''
    
        
    #Changing in this code, because the code above performs much less search in the list
    '''for pair in forbiddenActionState:
            pstate, paction = pair
            if (state == pstate and
                paction in allowed): 
                allowed.remove(paction)'''

    if(state in actionsByState):
        allowed = set(actionsByState[state]) - set(forbiddenActions)
    else:
        allowed = set(actions) - set(forbiddenActions)
        
    actionsByState[state] = allowed.copy()

    return list(allowed)

'''For each state that suffered change, verify for each state action in the original qtable if this action is in the new set
if it's not then delete that action from the original qtable, if it's then update the value. After that, add each new action
to the original qtable. The last else is for the states that are not in the original qtable.'''
################################################################################
def updateQwithASP(currentQTable, newQTable):    
    for newState in newQTable:
        if newState in set(currentQTable):
            actionsToDelete = []
            for action in currentQTable[newState]:
                if (action not in set(newQTable[newState])):
                    actionsToDelete.append(action)
                    
            for action in newQTable[newState]:
                if (action not in set(currentQTable[newState])):
                    #currentQTable[newState][action] = random() #0.0
                    currentQTable[newState][action] = newQTable[newState][action]
                #If the action is already on the QTable, then the algorithm does nothing
     
            for actionToDelete in actionsToDelete:
                del currentQTable[newState][actionToDelete]
            
        else:
            currentQTable[newState] = newQTable[newState]
            #for action in newQTable[newState]:
            #    currentQTable[newState][action] = newQTable[newState][action]
            

    return currentQTable
    
    #old code
    '''for s in set(currentQTable) & set(newQTable):
        for a in set(currentQTable[s]) & set(newQTable[s]):
            newQTable[s][a] += currentQTable[s][a]

    return newQTable'''
    

################################################################################
def rmsd(old, new):
    from math import pow, sqrt

    diff = 0

    if old != new:

        n = 0

        sBoth = set(old) & set(new)
        sOld = set(old) - sBoth
        sNew = set(new) - sBoth

        for s in sBoth:
            aBoth = set(old[s]) & set(new[s])
            aOld = set(old[s]) - aBoth
            aNew = set(new[s]) - aBoth

            for a in aBoth:
                n += 1
                diff += pow((old[s][a] - new[s][a]),2)

            for a in aOld:
                n += 1
                diff += pow(old[s][a],2)

            for a in aNew:
                n += 1
                diff += pow(new[s][a],2)

        for s in sOld:
            for a in old[s]:
                n += 1
                diff += pow(old[s][a],2)

        for s in sNew:
            for a in new[s]:
                n += 1
                diff += pow(new[s][a],2)

        diff = diff / n

    return diff

################################################################################
