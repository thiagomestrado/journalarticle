goalladder :- initial(S0),execute(S0,[
pass_t(str^ +,h2^ -),
pass_t(post^ +,ring^ -), 
pass_h(h2, ring^ -),
pass_h(sphere2,ring^ -),
pass_h(ring,h2^ +),
pass_h(sphere2,ring^ +),
pass_h(ring, h2^ -),
pass_h(ring, h1^ -),
pass_t(post^ -, ring^ +),
pass_h(h1, ring^ +),
pass_t(post^ +, ring^ +),
pass_h(h2, ring^ +),
pass_h(sphere2,ring^ +),
pass_h(ring,h2^ +),
pass_h(sphere2,ring^ -)
]).