### Fisherman's Folly Puzzle

#Imports
from enviromentDefinition import *
from random import random
import zmq
import enviromentDefinition
from enumerations import ElementFace, ElementType
from ElementsRelation import ElementsRelation

import pickle
from os import popen   
stateMapping = pickle.load(open("stateMapping.pck", "rb"))
#stateMapping = dict()

################################################################################
def nextState(state, action):
    terminalState = False
    reward  = rewardPerStep
    forbiddenStateActionPerformed = False
    forbiddenActionPerformed = False
    
    if(state not in (stateMapping)):
        stateMapping[state] = dict()
    
    if(action not in stateMapping[state]):        
        #Getting the mapped state and casting to the complex state, with the two chain lists.
        currentState = mapFromStringToState(state)
        #strintRelations, postRelations, ringRelations = currentState
        
        
        entitiesOfAction = getDescriptionOfAction(action)
        hostElement, elementReceivingAction, elementFace = entitiesOfAction
        '''print("\n\n Elemento: ", hostElement.name)
        print("Recebendo: ", elementReceivingAction.name)
        print("Face: ", elementFace)
    '''
        resultOfExecutedAction = hostElement.determineActionToExecute(elementReceivingAction, elementFace, currentState)  
        forbiddenStateActionPerformed, newState, forbiddenActionPerformed = resultOfExecutedAction 
        
        #newState = (stringRelations.copy(), postRelations.copy(), ringRelations.copy())
        mappedNewState = mapStateToString(newState)
        
        #adding the new discovered state to the dictionary
        if(not forbiddenActionPerformed): #If the action was a forbidden one, then it doesn't add to the mapping, because i have to send this response to the Agent.
            stateMapping[state][action] = mappedNewState
        
        #print("New state mapped: " + mappedNewState)
        '''print(state)
        print(mappedNewState)
        print(action)
        print(countNumberOfCrossings(newState))
        input()'''
    else:
        mappedNewState = stateMapping[state][action]
        if(mappedNewState == state):
            forbiddenStateActionPerformed = True
    
    if (forbiddenStateActionPerformed or forbiddenActionPerformed):
        #print("ACAO PROIBIDA")
        reward = minimalReward 
        
    elif isGoalStateFromString(mappedNewState):
        reward = maximumReward
        terminalState = True
        
    '''print(state)
    print(mappedNewState)
    print(action)   '''    
    #input()
        
    #showInformationAboutRelations(newState)
    
    return mappedNewState, reward, terminalState, forbiddenActionPerformed   


def countNumberOfCrossings(state):
    stringRelations, postRelations = state
    
    numberOfOcurrencesAfterAction = 0
    for relation in stringRelations:
        if(relation.elementInChain.orderInPuzzle == enviromentDefinition.ring.orderInPuzzle or
           relation.elementInChain.orderInPuzzle == enviromentDefinition.postHole1.orderInPuzzle or
           relation.elementInChain.orderInPuzzle == enviromentDefinition.postHole2.orderInPuzzle):
            numberOfOcurrencesAfterAction += 1
            
    return numberOfOcurrencesAfterAction


################################################################################
def mapStateToString(stateToMap):
    mappedState = ""
    
    #Separating the two list of crossings, one for each element of the system.
    stringCrossings, postCrossings = stateToMap 
    
    for elementRelationToConvert in stringCrossings:
        mappedState += getElementStateRepresentation(elementRelationToConvert)
    
    #mappedState += "-"
    mappedState += separador
    
    for elementRelationToConvert in postCrossings:
        mappedState += getElementStateRepresentation(elementRelationToConvert)
                
    
    return mappedState

################################################################################
def mapFromStringToState(mappedState):
    #print(mappedState)
    #stringCrossingsMapped, postCrossingsMapped, ringCrossingsMapped = mappedState.split("-")
    stringCrossingsMapped, postCrossingsMapped = mappedState.split(separador)
    
    stringCrossings = getComplexRelationReprsentation(stringCrossingsMapped, mainString)
    postCrossings = getComplexRelationReprsentation(postCrossingsMapped, post)
    
    complexState = (stringCrossings, postCrossings)
    
    return complexState


################################################################################
def getElementStateRepresentation(elementRelationToConvert):
    #Getting the int value of the enumeration that represents the element that is crossing the string or the post.
    #print(str(elementRelationToConvert.element) + " " + str(elementRelationToConvert.face) + " " + str(elementRelationToConvert.parent))
    
    elementConverted = ""
    
    if elementRelationToConvert.face == ElementFace.POSITIVE:
        elementConverted = "p"
    else:
        elementConverted = "n"
        
    elementConverted += str(elementRelationToConvert.elementInChain.orderInPuzzle)
        
    return elementConverted   

################################################################################
def getComplexRelationReprsentation(mappedStateToConvert, hostElement):
    numberOfPositionsToSplit = 2
    listOfElementsRelationMapped = [mappedStateToConvert[i:i+numberOfPositionsToSplit] 
                                   for i in range(0, len(mappedStateToConvert), numberOfPositionsToSplit)]
    elementChain = list()
    
    for relation in listOfElementsRelationMapped:
        face = ElementFace.POSITIVE if relation[0] == "p" else ElementFace.NEGATIVE
        elementInChain = getElementObjectFromInteger(int(relation[1]))
        
        elementChain.append(ChainRelations(hostElement, elementInChain, face))
        
    return elementChain


################################################################################
def getElementObjectFromInteger(orderOfElementInPuzzle):
    if(orderOfElementInPuzzle == 9):
        return post
    elif (orderOfElementInPuzzle == 0):
        return postHole1
    elif (orderOfElementInPuzzle == 1):
        return postHole2
    elif (orderOfElementInPuzzle == 2):
        return mainString
    elif (orderOfElementInPuzzle == 3):
        return ring
    elif (orderOfElementInPuzzle == 4):
        return disk1
    elif (orderOfElementInPuzzle == 5):
        return disk2
    elif (orderOfElementInPuzzle == 6):
        return sphere1
    else:
        return sphere2
    

################################################################################
def getDescriptionOfAction(action):
    actionDescription = actionsDescription[action]
    
    hostElement = getElementInActionDescription(actionDescription, False)
    elementInChain = getElementInActionDescription(actionDescription, True)
    elementFace = getFaceInActionDescription(actionDescription)
    
    completeDescriptionOfAction = (hostElement, elementInChain, elementFace)
    return completeDescriptionOfAction


################################################################################
def getElementInActionDescription(actionDescription, isElementBeingCrossed):
    indexToGetElement = 0
    #By default it gets the first position of the string because it's the element crossing, but if it's the other element, then it has to get the index 1
    if (isElementBeingCrossed):
        indexToGetElement = 1
    
    element = int(actionDescription[indexToGetElement])
    
    return getElementObjectFromInteger(element)
    
    
################################################################################    
def getFaceInActionDescription(actionDescription):
    face = int(actionDescription[2])
    
    if(face == 0):
        return ElementFace.NEGATIVE
    else:
        return ElementFace.POSITIVE
        
    

################################################################################
def initState():
    #message = 'InitialState'
    message = mapStateToString(initialState)
    return message


################################################################################
def envConf():
    message = ''

    for action in actions:
        message += str(action) + ' '

    message += str(maximumReward) + ' ' + str(minimalReward)

    return message


def saveStateMapping():   
    from pickle import dump
    from os import popen
    
    fname = 'stateMapping.pck'
    with open(fname,'wb') as f:
        dump(stateMapping, f)
        
def loadStateMapping():
    import pickle
    from os import popen   
    mapping = pickle.load(open("stateMapping.pck", "rb"))
    return mapping 

################################################################################
def stationaryEnvironment(socket, maxEpisodes):
    print("Rope Ladder Puzzle")

    e = 0
    numberOfEpisodesExecuted = 0
    for _ in range(maxEpisodes):
        end = False
        ringCrossedPost1Negative = False
        ringCrossedPost2Positive = False #Change to True if the puzzle starts from the fifth action.
        lastAction = -1

        while not end:
            message = socket.recv_string()
            numberOfEpisodesExecuted += 1
            
            #print("Phase: %s" % message)

            if message == 'init':
                message = initState()

            elif message == 'conf':
                message = envConf()
                e += 1

            elif message == 'END':
                end = True

            else:
                #The action to be performed came from the agent. The agent code decides the action.
                state, action = message.split()
                action = int(action)

                #This is an uglyyyyyy code, I have to change this code, it's only for test.
		#lastAction == action or
                if((action == 4 and ringCrossedPost1Negative == False) or #If the ring is not crossing the Post1 in the Negative, then I can't cross the ring through the Post1 in the Positive.
                   (action == 7 and ringCrossedPost2Positive == False) or #If the ring is not crossing the Post2 in the Positive, then I can't cross the ring through the Post2 in the Negative.
                   #or action == 18 or action == 19
                   ((action == 6 or action == 7) and ringCrossedPost1Negative == True) or #If the ring is crossing the Post1 in the extreme, then I can't cross the Post2 in any direction, nor can I cross the Sphere2 through the Ring in any direction.
                   # or action == 16 or action == 17
                   ((action == 4 or action == 5) and ringCrossedPost2Positive == True)): #If the ring is crossing the Post2 in the extreme, then I can't cross the Post1 in any direction, nor can I cross the Sphere1 through the Ring in any direction.
                    message = str(state) + ' '
                    message += str(minimalReward) + ' '
                    message += str(False) + ' '
                    message += str(False)
                else:
                    newState, reward, terminalState, forbiddenActionPerformed = nextState(state, action)
                    
                    if(reward > minimalReward):
                        if(action == 5):
                            ringCrossedPost1Negative = True
                        elif(action == 6):
                            ringCrossedPost2Positive = True
                        elif(action == 4): #Inverse of action 5
                            ringCrossedPost1Negative = False
                        elif(action == 7): #Inverse of action 6
                            ringCrossedPost2Positive = False
                        elif(action == 2 or action == 3): #Action with post2 and ring, so it clears past actions with the extreme.
                            ringCrossedPost1Negative = False
                        elif(action == 0 or action == 1): #Action with post1 and ring, so it clears past actions with the extreme.
                            ringCrossedPost2Positive = False
                        
                    message = str(newState) + ' '
                    message += str(reward) + ' '
                    message += str(terminalState) + ' '
                    message += str(forbiddenActionPerformed)

                lastAction = action

            #print("About this execution: %s" % message)
            socket.send_string(message)
            
        if((numberOfEpisodesExecuted % 1000) == 0):
            saveStateMapping()

################################################################################
def runEnvironment(episodes, trials):
    import zmq
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    conn = "tcp://*:" + str(port)
    socket.bind(conn)
    print("Environment is up")

    #global postRelations, stringRelations

    for t in range(trials):
        print("Trial: ", t)
        stringRelations = initialState[0]
        postRelations = initialState[1]
        stationaryEnvironment(socket, episodes)


################################################################################
if __name__ == '__main__':
    runEnvironment(episodes, trials)


################################################################################
