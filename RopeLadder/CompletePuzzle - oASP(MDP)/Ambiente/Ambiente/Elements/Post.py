from Elements.Element import *
from enumerations import ElementType, ElementFace
from enviromentDefinition import *
import enviromentDefinition
from utilMethods import *
import utilMethods

class Post(Element):
    def __init__(self, nameOfElement, initialIndex, orderInPuzzle):
        Element.__init__(self, ElementType.POST, orderInPuzzle, True)
        self.name = nameOfElement
        self.initialIndex = initialIndex
        self.orderInPuzzle = orderInPuzzle        
    
    def printElementType(self):
        print("This is an element of type POST")
        
    def executeAction(self, elementReceivingAction, elementFace, currentState):   
        if elementReceivingAction.elementType == ElementType.RING:
            entitiesOfAction = (self, elementReceivingAction, elementFace)
            
            continueExecution = self.verifyRestrictions(elementReceivingAction, elementFace, currentState)
            if(not continueExecution):
                return (True, currentState, False) 
            
            util = UtilMethods()
            complexPrologResponse = util.executePrologCommand(currentState, entitiesOfAction)
            actionExecuted, newState = complexPrologResponse
            
            if(actionExecuted):
                if(not self.verifyNumberOfOcurrences(elementReceivingAction, currentState, newState)):
                    return (True, currentState, False)                
                
                if(newState != currentState):
                    return (False, newState, False)

            return (True, currentState, False)   
        
        return (True, currentState, True)
    
    def verifyRestrictions(self, elementReceivingAction, elementFace, currentState):   
        stringRelations, postRelations = currentState
        
        numberOfRingCrossings = 0
        for relation in stringRelations:
            if(relation.elementInChain.orderInPuzzle == enviromentDefinition.ring.orderInPuzzle):
                numberOfRingCrossings += 1
        
        '''if(numberOfRingCrossings == enviromentDefinition.numberOfStringCrossingRing):
            return False
        '''
                
        ringIsBeingCrossedByPost = False
        #Verify if the post is crossing the ring and the face of the crossing. If this is true, then it can't continue, because it can't cross the ring in the same direction more than once.
        for relation in postRelations:
            if(relation.elementInChain.orderInPuzzle == elementReceivingAction.orderInPuzzle):
                ringIsBeingCrossedByPost = True
                '''if(relation.face == ElementFace.POSITIVE):
                    if(self.orderInPuzzle == enviromentDefinition.postHole1.orderInPuzzle and
                       elementFace == ElementFace.NEGATIVE):
                        return False
                    elif(self.orderInPuzzle == enviromentDefinition.postHole2.orderInPuzzle and
                         elementFace == ElementFace.POSITIVE):
                        return False  '''                        
                    
        '''if(not ringIsBeingCrossedByPost and
           elementFace == ElementFace.NEGATIVE):
            return False'''
           
        return True
    
    def verifyNumberOfOcurrences(self, elementReceivingAction, currentState, newState):
        newStringRelations, postRelations = newState
        
        numberOfOcurrencesAfterAction = 0
        for relation in newStringRelations:
            '''relation.elementInChain.orderInPuzzle == enviromentDefinition.postHole1.orderInPuzzle or
               relation.elementInChain.orderInPuzzle == enviromentDefinition.postHole2.orderInPuzzle or'''
            if(relation.elementInChain.orderInPuzzle == enviromentDefinition.ring.orderInPuzzle):
                numberOfOcurrencesAfterAction += 1
        
        postIsCrossingRing = False
        for postRelation in postRelations:
            if(postRelation.elementInChain.orderInPuzzle == enviromentDefinition.ring.orderInPuzzle):
                postIsCrossingRing = True
        
        if(numberOfOcurrencesAfterAction > enviromentDefinition.numberOfStringCrossingRing):
            return False
        #This is == because if the Ring is being crossed in the limit, then it's impossible to cross the Ring with the Post.
        elif(numberOfOcurrencesAfterAction == enviromentDefinition.numberOfStringCrossingRing and
             postIsCrossingRing):
            return False
        
        return True
            