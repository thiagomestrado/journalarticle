%--- Rope lader data -------------------------------------
% Defining the elements of the Rope Ladder Puzzle -----
% Seven elements: Post, String, Ring, Disk1, Disk2, Sphere1 and Sphere2
% The post here is considered only one, but two holes are considered

% Long Elements
long(str). 
long(post).

% Disks
disk(disk1).      
disk(disk2).

% Spheres
sphere(sphere1).  
sphere(sphere2).

% Elements with Holes
hole(ring).
posthole(h1).     
posthole(h2).
hole(S) :- sphere(S).
hole(PH):-posthole(PH).

% Regular Elements. Only the disk because this puzzle doesnt have base
regular(D) :- disk(D).

% Actions that can never be executed
cannotPass(D,S,[])        :-disk(D),sphere(S).
cannotPass(D,ring,[])     :-disk(D).

cannotPass(post,S,[])     :-sphere(S).
cannotPass(post,PH,[])    :-posthole(PH).

cannotPass(S,PH,[])       :-sphere(S),posthole(PH).
cannotPass(S,S1,[])       :-sphere(S), sphere(S1).
cannotPass(S,ring,[post]) :-sphere(S).

cannotPass(PH,S,[])       :-sphere(S),posthole(PH).
cannotPass(ring,S,[])     :-sphere(S).

% Actions that cannot be executed depending on the conditions of the puzzle.
cannotPass(H,H,_) 	  :- hole(H).
cannotPass(X,H,Z):- Z\=[],getsubset(Z,Y),Y\=Z,cannotPass(X,H,Y),!.

% INITIAL ------------------------------------------------

% Older initial code. I adapted to work with hold
% initial([
  % linked(str^ -, disk1)=true,
  % linked(str^ +, disk2)=true,
  % chain(str)=[str^ -, sphere1^ +, h1^ +, h2^ +, h1^ -, h2^ +, sphere2^ +, str^ +],
  % chain(post)=[post^ -, ring^ +, post^ +],
  % linked(post^ -, h1)=true,
  % linked(post^ +, h2)=true
% ]).

initial([
  holds(linked(str^ -, disk1), true),
  holds(linked(str^ +, disk2), true),
  holds(linked(post^ -, h1), true),
  holds(linked(post^ +, h2), true),
  holds(chain(str),[str^ -, sphere1^ +, h1^ +, h2^ +, h1^ -, h2^ +, sphere2^ +, str^ +]),
  holds(chain(post),[post^ -, ring^ +, post^ +])
]).

% If it is the first interaction, then it just has to perform the action on the default initial state of the puzzle.
start(ACTION) :- 
	initial(S0),
	add_bundles(S0,S1),
	execute(S1, ACTION).

% If it is not the first interaction, then it has to send the current state of the puzzle and the action to be executed.
start(STATE, ACTION) :-  
	S0 = STATE,	
	add_bundles(S0,S1),
	execute(S1, ACTION).


% GOAL ---------------------------------------------------------
% the ring is not crossed by anything
% older: is_goal(State):-   \+ (member(chain(_)=L,State),member(ring^ _, L)). 
is_goal(S) :- crossedBy(ring,[],S).
