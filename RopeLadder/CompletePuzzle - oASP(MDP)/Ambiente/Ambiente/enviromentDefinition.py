###############################################################################

## Imports
from enumerations import  ElementFace
from Elements.Disk import *
from Elements.MainString import *
from Elements.Post import *
from Elements.Ring import *
from Elements.Sphere import *
from chainRelations import ChainRelations


# Creating the elements of the puzzle.
post = Post("post", 0, 9)
postHole1 = Post("h1", 0, 0)
postHole2 = Post("h2", 16, 1)
mainString = MainString("str", 32, 2)
ring = Ring("ring", 48, 3)
disk1 = Disk("disk1", 64, 4)
disk2 = Disk("disk2", 80, 5)
sphere1 = Sphere("sphere1", 96, 6)
sphere2 = Sphere("sphere2", 112, 7)


# Initial State
# For now the initial state is going to be a constant state, pre-defined by the programmer.
# Creating Initial Relationships

stringRelations = [ChainRelations(mainString, mainString, ElementFace.NEGATIVE),
                   ChainRelations(mainString, sphere1, ElementFace.POSITIVE),
                   ChainRelations(mainString, postHole1, ElementFace.POSITIVE),
                   ChainRelations(mainString, postHole2, ElementFace.POSITIVE),
                   ChainRelations(mainString, postHole1, ElementFace.NEGATIVE),
                   ChainRelations(mainString, postHole2, ElementFace.POSITIVE),
                   ChainRelations(mainString, sphere2, ElementFace.POSITIVE),
                   ChainRelations(mainString, mainString, ElementFace.POSITIVE)]

#Post is crossing the Ring.
postRelations = [ChainRelations(post, post, ElementFace.NEGATIVE),
                 ChainRelations(post, ring, ElementFace.POSITIVE),
                 ChainRelations(post, post, ElementFace.POSITIVE)]

initialState =  (stringRelations.copy(), postRelations.copy())
currentComplexState = (stringRelations.copy(), postRelations.copy())


# Goal State
# The Goal State is a state where the Ring isn't being crossed by any other element.
def isGoalState(completeStateToVerify):
    stringRelationsToVerify, postRelationsToVerify = completeStateToVerify
    
    for relation in stringRelationsToVerify: #In the position zero it's the string relations
        if int(relation.elementInChain.orderInPuzzle) == int(ring.orderInPuzzle):
            return False #If the ring is being crossed by the MainString, then it's not the goal state.
    
    for relation in postRelationsToVerify: #In the position one it's the post relations
        if int(relation.elementInChain.orderInPuzzle) == int(ring.orderInPuzzle): 
            return False #If the ring is being crossed by the Post, then it's not the goal state.
        
    #If the ring isn't being crossed by the MainString nor the Post, then it's free from the system.
    return True     

def isGoalStateFromString(stringStateToVerify):
    if(stringStateToVerify.find("3") > -1):
        return False

    return True
    
numberOfElements = 8
numberOfFaces = 2


totalNumberOfActions = numberOfElements * numberOfElements * numberOfFaces; #128 possible actions, 16 per element

#In this implementation the actions are a set that specifies the element that is going to cross, a element that is going to be crossed and the face of interaction.    
'''actions = list(range(0, totalNumberOfActions))
actionsDescription = []

def determineListOfActionsBasedOnElements():
    for i in range(0, numberOfElements):
            for a in range(0, numberOfFaces):
                for b in range(0, numberOfElements):
                    actionDescription = "" + str(i) + str(b) + str(a) + "" #The crossing element, the element being crossed and the face.
                    actionsDescription.append(actionDescription)                    
                    
determineListOfActionsBasedOnElements()
'''

#This code is just for test, to see how the algorithm behaves when it just performs possible actions
totalNumberOfActions = 28
actions = list(range(0, totalNumberOfActions))
actionsDescription = []

#Post1 Actions
post1TrhoughRingPos = "" + str(postHole1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
post1TrhoughRingNeg = "" + str(postHole1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(post1TrhoughRingPos) #0
actionsDescription.append(post1TrhoughRingNeg) #1

#Post2 Actions
post2TrhoughRingPos = "" + str(postHole2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
post2TrhoughRingNeg = "" + str(postHole2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(post2TrhoughRingPos) #2
actionsDescription.append(post2TrhoughRingNeg) #3

#Ring Actions in Post1
ringTrhoughPost1Pos = "" + str(ring.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.POSITIVE.value))
ringTrhoughPost1Neg = "" + str(ring.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(ringTrhoughPost1Pos) #4
actionsDescription.append(ringTrhoughPost1Neg) #5

#Ring Actions in Post2
ringTrhoughPost2Pos = "" + str(ring.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.POSITIVE.value))
ringTrhoughPost2Neg = "" + str(ring.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(ringTrhoughPost2Pos) #6
actionsDescription.append(ringTrhoughPost2Neg) #7


#Disk1 Actions in Post1
disk1TrhoughPost1Pos = "" + str(disk1.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.POSITIVE.value))
disk1TrhoughPost1Neg = "" + str(disk1.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(disk1TrhoughPost1Pos) #8
actionsDescription.append(disk1TrhoughPost1Neg) #9
'''
#Disk1 Actions in Post2
disk1TrhoughPost2Pos = "" + str(disk1.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.POSITIVE.value))
disk1TrhoughPost2Neg = "" + str(disk1.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(disk1TrhoughPost2Pos) #10
actionsDescription.append(disk1TrhoughPost2Neg) #11


#Disk2 Actions in Post1
disk2TrhoughPost1Pos = "" + str(disk2.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.POSITIVE.value))
disk2TrhoughPost1Neg = "" + str(disk2.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(disk2TrhoughPost1Pos) #12
actionsDescription.append(disk2TrhoughPost1Neg) #13
'''

#Disk2 Actions in Post2
disk2TrhoughPost2Pos = "" + str(disk2.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.POSITIVE.value))
disk2TrhoughPost2Neg = "" + str(disk2.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(disk2TrhoughPost2Pos) #14
actionsDescription.append(disk2TrhoughPost2Neg) #15


#Sphere1 Actions
sphere1TrhoughRingPos = "" + str(sphere1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere1TrhoughRingNeg = "" + str(sphere1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere1TrhoughRingPos) #16
actionsDescription.append(sphere1TrhoughRingNeg) #17

#Sphere2 Actions
sphere2TrhoughRingPos = "" + str(sphere2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere2TrhoughRingNeg = "" + str(sphere2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere2TrhoughRingPos) #18
actionsDescription.append(sphere2TrhoughRingNeg) #19



#Sphere1 Impossible Actions in PostHole1
sphere1TrhoughPost1Pos = "" + str(sphere1.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere1TrhoughPost1Neg = "" + str(sphere1.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere1TrhoughPost1Pos) #20
actionsDescription.append(sphere1TrhoughPost1Neg) #21

#Sphere2 Actions in PostHole2
sphere2TrhoughPost1Pos = "" + str(sphere2.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere2TrhoughPost1Neg = "" + str(sphere2.orderInPuzzle) + str(postHole1.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere2TrhoughPost1Pos) #22
actionsDescription.append(sphere2TrhoughPost1Neg) #23


#Sphere1 Impossible Actions in PostHole2
sphere1TrhoughPost2Pos = "" + str(sphere1.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere1TrhoughPost2Neg = "" + str(sphere1.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere1TrhoughPost2Pos) #24
actionsDescription.append(sphere1TrhoughPost2Neg) #25

#Sphere2 Actions
sphere2TrhoughPost2Pos = "" + str(sphere2.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.POSITIVE.value))
sphere2TrhoughPost2Neg = "" + str(sphere2.orderInPuzzle) + str(postHole2.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(sphere2TrhoughPost2Pos) #26
actionsDescription.append(sphere2TrhoughPost2Neg) #27


#Disk1 Impossible Actions in Ring 
disk1TrhoughRingPos = "" + str(disk1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
disk1TrhoughRingNeg = "" + str(disk1.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(disk1TrhoughRingPos) #28
actionsDescription.append(disk1TrhoughRingNeg) #29


#Disk2 Actions in Post1
disk2TrhoughRingPos = "" + str(disk2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.POSITIVE.value))
disk2TrhoughRingNeg = "" + str(disk2.orderInPuzzle) + str(ring.orderInPuzzle) + str((ElementFace.NEGATIVE.value))
actionsDescription.append(disk2TrhoughRingPos) #30
actionsDescription.append(disk2TrhoughRingNeg) #31

###############################################################################
# transition probabilities
oposite = 0
orthopos = 0
orthoneg = 0

aO = [0, 0, 0]
aP = [0, 0, 0]
aN = [0, 0, 0]

###############################################################################
# reward values
minimalReward  = -100 #-1000 #If each action in that episode is the bad one, then the total reward will be -1000.
maximumReward  = 1000 #1000
rewardPerStep = -1 #-1

###############################################################################
episodes = 6000
trials = 30
port = 5000

###############################################################################
separador = "b"

###############################################################################
#This, together with the number of steps, is the limiting factor of the size of the string. With less steps, less crossings.
numberOfStringCrossingRing = 6
numberOfStringCrossingPosts = 8
allowedNumberOfKnots = 0

###############################################################################
puzzleDefinitionProlog = "Oracle/ropeLadderDefinition.pl"
puzzlePlannerProlog = "Oracle/puzzlePlanner.pl"
